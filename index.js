const express = require('express');
const axios = require('axios');
const cors = require('cors');

const app = express();

app.use(cors());

app.use((req, res) => {
  axios.request({
    url: 'https://www.cityfalcon.ai' + req.path,
    method: req.method,
    params: req.query,
    data: req.body,
    headers: Object.entries(req.headers),
  }).then(({ data, headers }) => {
    Object.entries(headers).forEach(([key, value]) => {
      res.setHeader(key, value);
    });
    res.json(data);
  }, (e) => {
    res.json(e.response.data);
  });
});

app.listen(process.env.PORT || 8000, () => console.log('Started'));
